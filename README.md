## Notes and materials for my lectures for the course on Mobile and Cyber-Physical Systems
Since AA 16/17 I give some lectures regarding the course on Mobile and Cyber Physical Systems.

The topics are the following:
  * Introduction to the Thingspeak IoT platform
  * Integration of ThingSpeak with REST services. Example with MongoDB.
  * Signal sampling with Arduino. Using the Arduino timer to obtain sub-milllisecond sampling and FFT.

This repository hosts the trasparencies shown during the lectures, and the laboratory materila for practicing with ThingSpeak and with Arduino.

During health emergency in 2020 I recorded the video lectures: they are available (only for the students of the University of Pisa) in my Google Drive using the following [link](https://drive.google.com/drive/folders/1gZ-h9C98agp7oKEVolVVggGd_rZLCMtR?usp=sharing). 
