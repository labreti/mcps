/*
 * ADC clock is prescaled w.r.t. the CPU clock, and
 * analog conversion takes 13 ADC clock cycles (25 the
 * first time). The left column indicates maximum sampling
 * periods, without taking account loop control and assignments
 * =========================================================
 * prescale | b2 | b1 | b0 | conversion | max sampling freq. 
 *          |    |    |    |   delay    |   w/ 16MHz clock 
 * ---------------------------------------------------------
 *     2    |  0 |  0 |  0 |   1.6 us   | 615 KHz
 *     2    |  0 |  0 |  1 |   1.6 us   | 615 KHz
 *     4    |  0 |  1 |  0 |   3.2 us   | 308 KHz
 *     8    |  0 |  1 |  1 |   6.5 us   | 154 KHz
 *    16    |  1 |  0 |  0 |    13 us   |  77 KHz
 *    32    |  1 |  0 |  1 |    26 us   |  38 KHz
 *    64    |  1 |  1 |  0 |    52 us   |  19 KHz
 *   128    |  1 |  1 |  1 |   104 us   |  10 KHz
*/
#define DIM 500
#include <arduinoFFT.h>
arduinoFFT FFT = arduinoFFT();
// funzione di utility
void set_prescaler(int b2, int b1, int b0) {
  int ADPS[]={b0,b1,b2};
  int i=0;
  for (i=0; i<3; i++) {
    if ( ADPS[i] ) { ADCSRA |= _BV(i); }
    else { ADCSRA &= ~(_BV(i)); }
  }
}

void setup() {
  Serial.begin(57600);
  set_prescaler(0,1,1); // impostazione del prescaler
  // Generatore onda quadra a 490.2 Hz sul pin 3
  pinMode(3,OUTPUT);
  analogWrite(3, 128);
}

int R[DIM]; // Buffer dati
int threshold=500;

void loop() {
  int i=0;
  unsigned long t0;
  float period;
  float rate;
// aggancia la soglia
  while ( analogRead(A0) > threshold ) { }
  while ( analogRead(A0) < threshold ) { }
  t0=micros(); // tempo in microsecondi
  for (i=0; i<DIM;i++) { R[i]=analogRead(A0); } // Campionamento
  period=((float)(micros()-t0))/DIM;    // Calcolo periodo in microsec
  rate=period/10.0; // 1 punto = 10us
  for (int i=0; i<DIM; i++) {
    int k=(int) i*rate;
    Serial.println(R[k]);
  }
  delay(1000);
}
