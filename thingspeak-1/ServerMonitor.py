import time, sys, httplib, json, psutil
if len(sys.argv) != 2:                                 # Controllo parametri
  print("\nUsage: "+sys.argv[0]+" <writekey>\n")
  exit(1)
writekey=sys.argv[1]
while True:
  body = json.JSONEncoder().encode(
    { "field1": psutil.cpu_percent(),                  # percentuale cpu occupata
      "field2": psutil.virtual_memory().percent,       # memoria usata
      "field3": psutil.net_io_counters().packets_sent, # pacchetti inviati
      "field4": psutil.net_io_counters().packets_recv, # pacchetti ricevuti
      "key":writekey}  
  )
  print body
  headers = {"Content-type": "application/json"} 
  conn = httplib.HTTPConnection("api.thingspeak.com")
  try:                                                 # Gestione delle eccezioni (errori)
    conn.request("POST", "/update", body, headers)
    conn.close()
  except:                                              # In caso di errore...
    print "Connessione fallita: dato non registrato." 
  time.sleep(60) 

